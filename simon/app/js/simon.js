const simon = {
  onReady: function () {
    $('[data-name="playbtn"]').on('click', simon.startGame);
    $('[data-name="gamebtn"]').on('click', simon.playerClicked);

    simon.populateArr($('[data-name="gamebtn"]'), simon.blocksArr);

    simon.refreshLvl();
  },

  clicks: 0,

  level: 0,

  blocksArr: [],

  gamearr: [],

  playerarr: [],

  isPlayPressed: false,

  isColorsPlaying: false,

  startGame: function (e) {
    e.preventDefault();

    if (!simon.isColorsPlaying) {
      simon.isPlayPressed = true;
      simon.level = 1;
      simon.refreshLvl();
      $('[data-name="gameover-text"]').removeClass('active');
      simon.resetGame();
      simon.addRandomColor(simon.gamearr);
      simon.playColors();
    }
  },

  resetGame: function () {
    simon.clicks = 0;
    simon.level = 0;
    simon.refreshLvl();
    simon.gamearr = [];
    simon.playerarr = [];
  },

  playerClicked: function (e) {

    e.preventDefault();

    if (!simon.isPlayPressed) {
      return;
    }

    if (simon.isColorsPlaying) {
      return;
    }

    simon.clicks++;

    simon.playerarr.push($(this).attr('data-color'));

    if (simon.playerarr[simon.clicks - 1] == simon.gamearr[simon.clicks - 1]) {
      if (simon.clicks == simon.gamearr.length) {
        simon.addRandomColor();
        simon.playColors(false);
      }
    } else {
      simon.gameOver();
    }
  },

  playColors: function () {

    simon.isColorsPlaying = true;

    let i = 0;

    function playColor() {
      window.setTimeout(function () {
        $('[data-color="' + simon.gamearr[i] + '"]').addClass('lighten');

        window.setTimeout( function () {
          $('[data-color="' + simon.gamearr[i] + '"]').removeClass('lighten');
          i++;
        }, 700);

        if (i < simon.gamearr.length) {
          playColor();
        } else {
          simon.isColorsPlaying = false;
        }
      }, 1000);
    }

    playColor();
    simon.clicks = 0;
    simon.level++;
    simon.refreshLvl();
    simon.playerarr = [];
  },

  addRandomColor: function (arr) {
    let num = simon.getRaandomNum(0, simon.blocksArr.length);

    simon.gamearr.push(simon.blocksArr[num]);
  },

  getRaandomNum: function (min, max) {
    return Math.floor(Math.random() * (max - min) + min);
  },

  populateArr: function (elemArr, blockArr) {

    for (let i = 0; i < elemArr.length; i++) {
      blockArr.push(elemArr[i].getAttribute('data-color'));
    }
  },

  gameOver: function () {
    $('[data-name="gameover-text"]').addClass('active').append(' your lvl ' + simon.level);
    simon.resetGame();
    simon.isPlayPressed = false;
  },

  refreshLvl: function () {
    $('[data-name="level"]').text(simon.level);
  }
}

$(document).ready(simon.onReady());
