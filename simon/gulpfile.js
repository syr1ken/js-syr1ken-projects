const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const stylus = require('gulp-stylus');
const imagemin = require('gulp-imagemin');
const useref = require('gulp-useref');
const gulpif = require('gulp-if');
const minifyCss = require('gulp-clean-css');
const uglifyJs = require('gulp-uglify-es').default;
const autoprefixer = require('gulp-autoprefixer');

// Move fonts to dist
gulp.task('fonts', function () {
  return gulp.src('app/fonts/**/*')
    .pipe(gulp.dest('dist/fonts'))
});

// Preprocces stylus files on src
gulp.task('stylus', function (done) {
  return gulp.src('app/stylus/style.stylus')
    .pipe(stylus())
    .pipe(gulp.dest('app/css'))
    .pipe(autoprefixer())
    .pipe(browserSync.stream());

  done();
});

// Move optimized .html to dist and minify css
gulp.task('html', function () {
  return gulp.src('app/*.html')
    .pipe(useref())
    .pipe(gulpif('*.css', minifyCss()))
    .pipe(gulp.dest('dist'));
});

// Minify Css
gulp.task('minify-css', function (done) {
  return gulp.src('app/**/*.css')
    .pipe(minifyCss())
    .pipe(gulp.dest('dist'));
});

// Uglify JS
gulp.task('uglify-js', function (done) {
  return gulp.src('app/**/*.js')
    .pipe(uglifyJs())
    .pipe(gulp.dest('dist'));
});

// Minify images
gulp.task('images', function () {
  return gulp.src('app/img/**/*.+(png|jpg|gif|svg)')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/img'))
});

// Live reload server
gulp.task('browser-sync', function(done) {
  browserSync.init({
    server: {
      baseDir: 'app',
    },
    browser: 'chromium',
    notify: false
  });

  browserSync.watch('app').on('change', browserSync.reload);
  done();
});

// Watching and live reloading GULP v4
gulp.task('watch', gulp.series('stylus', 'browser-sync', function (done) {
  gulp.watch('app/stylus/**/*.stylus', gulp.series('stylus'));
  done();
}));

// Move src files to dist
gulp.task('dist', gulp.series('fonts', 'html', 'images', 'minify-css', 'uglify-js', function (done) {
  done();
}));

// Create directories structure for project
gulp.task('directories', function (done) {
  return gulp.src('*.*', {read: false})
    .pipe(gulp.dest('./app'))
    .pipe(gulp.dest('./app/img'))
    .pipe(gulp.dest('./app/fonts'))
    .pipe(gulp.dest('./app/css'))
    .pipe(gulp.dest('./app/stylus'))
    .pipe(gulp.dest('./app/js'))
    .pipe(gulp.dest('./dist'));
  done();
});

gulp.task('default', gulp.series('stylus', 'dist', function (done) {
  done();
}));
