const rps = {
  onReady: function () {
    $('[data-type="game-btn"]').on('click', rps.btnclick);
    $('[data-type="result-block"]').css('display', 'none');

    rps.populateArr($('[data-type="game-btn"]'), rps.gameELements);

  },

  gameELements: [],

  btnclick: function (e) {
    e.preventDefault();

    // Remove classes
    $('[data-type="comp-result"]').removeClass('active');
    $('[data-type="player-result"]').removeClass('active');
    $('[data-type="text-result"]').text('');

    // Find random number
    let rand = rps.getRandomNum(0, rps.gameELements.length);

    // Add active class to comp-result with coresponding rand number in rps.gameELements
    $('[data-type="comp-result"]').get(rand).classList.add('active');

    // Add active class to player-result corisponding to pressed button
    $('[data-type="player-result"][data-name="' + $(this).attr('data-name') + '"]').addClass('active');

    rps.gamePlay($('[data-type="comp-result"]').get(rand).getAttribute('data-name'), $(this).attr('data-name'));

    // Add display vlock to result-block
    $('[data-type="result-block"]').css('display', 'block');

  },

  gamePlay: function (comp, player) {
    if (player == 'rock') {
      if (comp == 'rock') {
        $('[data-type="text-result"]').text('It\'s a draw');
      } else if (comp == 'papper') {
        $('[data-type="text-result"]').text('You lose');
      } else if (comp == 'scissors') {
        $('[data-type="text-result"]').text('You win');
      }
    } else if (player == 'papper') {
      if (comp == 'rock') {
        $('[data-type="text-result"]').text('You win');
      } else if (comp == 'papper') {
        $('[data-type="text-result"]').text('It\'s a draw');
      } else if (comp == 'scissors') {
        $('[data-type="text-result"]').text('You lose');
      }
    } else if (player == 'scissors') {
      if (comp == 'rock') {
        $('[data-type="text-result"]').text('You lose');
      } else if (comp == 'papper') {
        $('[data-type="text-result"]').text('You win');
      } else if (comp == 'scissors') {
        $('[data-type="text-result"]').text('It\'s a draw');
      }
    }
  },

  getRandomNum: function (min, max) {
    return Math.floor(Math.random() * (max - min) + min);
  },

  populateArr: function (elemArr, blockArr) {
    for (let i = 0; i < elemArr.length; i++) {
      blockArr.push(elemArr[i].getAttribute('data-name'));
    }
  }
}

$(document).ready(rps.onReady());
