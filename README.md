# JS Projects

This is my repository where I upload some of my solutions for some challenges. Some of the web apps that you can see below I created by myself just for practice.

#### Dice Roll

Created with: pug, gulp, stylus, JS(ES5), Bootstrap, Font Awesome  
[Repo](https://gitlab.com/syr1ken/js-syr1ken-projects/-/tree/main/dice)  
[Demo](https://syr1ken.gitlab.io/js-syr1ken-projects/dice/dist/index.html)

#### Budget App

Created with: pug, gulp, stylus, JS(ES5), Bootstrap, Font Awesome  
[Repo](https://gitlab.com/syr1ken/js-syr1ken-projects/-/tree/main/budgetApp)  
[Demo](https://syr1ken.gitlab.io/js-syr1ken-projects/budgetApp/dist/index.html)

#### Love Calculator

Created with: pug, gulp, less, JS(ES5), Bootstrap, Font Awesome  
[Repo](https://gitlab.com/syr1ken/js-syr1ken-projects/-/tree/main/loveCalc)  
[Demo](https://syr1ken.gitlab.io/js-syr1ken-projects/loveCalc/dist/index.html)

#### Calculator

Created with: pug, gulp, less, JS(ES5), Bootstrap, Font Awesome  
[Repo](https://gitlab.com/syr1ken/js-syr1ken-projects/-/tree/main/calc)  
[Demo](https://syr1ken.gitlab.io/js-syr1ken-projects/calc/dist/index.html)

#### Piglatin

Created with: pug, gulp, sass, JS+JQuery, Bootstrap, Font Awesome  
[Repo](https://gitlab.com/syr1ken/js-syr1ken-projects/-/tree/main/pigLatin)  
[Demo](https://syr1ken.gitlab.io/js-syr1ken-projects/pigLatin/dist/index.html)

#### Recipe App

Created with: pug, webpack, sass, JS(ES6)  
[Repo](https://gitlab.com/syr1ken/js-syr1ken-projects/-/tree/main/recipeApp)  
[Demo](https://syr1ken.gitlab.io/js-syr1ken-projects/recipeApp/dist/index.html)

#### Weather App

Created with: pug, webpack, sass, JS(ES6)  
[Repo](https://gitlab.com/syr1ken/js-syr1ken-projects/-/tree/main/weatherApp)  
[Demo](https://syr1ken.gitlab.io/js-syr1ken-projects/weatherApp/dist/index.html)

#### Simon

Created with: HTML5, stylus, JS+JQuery, Bootstrap  
[Repo](https://gitlab.com/syr1ken/js-syr1ken-projects/-/tree/main/simon)  
[Demo](https://syr1ken.gitlab.io/js-syr1ken-projects/simon/dist/index.html)

#### Rock Papper Scissors

Created with: HTML5, less, JS+JQuery, Bootstrap  
[Repo](https://gitlab.com/syr1ken/js-syr1ken-projects/-/tree/main/rps)  
[Demo](https://syr1ken.gitlab.io/js-syr1ken-projects/rps/dist/index.html)

#### Form Validate

Created with: pug, CSS3, JS(ES6)  
[Repo](https://gitlab.com/syr1ken/js-syr1ken-projects/-/tree/main/formValidate)  
[Demo](https://syr1ken.gitlab.io/js-syr1ken-projects/formValidate/dist/index.html)

#### Menu Slider Modal

Created with: pug, CSS3, JS(ES6)  
[Repo](https://gitlab.com/syr1ken/js-syr1ken-projects/-/tree/main/menuSliderModal)  
[Demo](https://syr1ken.gitlab.io/js-syr1ken-projects/menuSliderModal/dist/index.html)

#### Movie Seat Booking

Created with: pug, CSS3, JS(ES6)  
[Repo](https://gitlab.com/syr1ken/js-syr1ken-projects/-/tree/main/movieSeatBooking)  
[Demo](https://syr1ken.gitlab.io/js-syr1ken-projects/movieSeatBooking/dist/index.html)

#### Video Player

Created with: pug, CSS3, JS(ES6)  
[Repo](https://gitlab.com/syr1ken/js-syr1ken-projects/-/tree/main/videoPlayer)  
[Demo](https://syr1ken.gitlab.io/js-syr1ken-projects/videoPlayer/dist/index.html)

#### Dom Array Methods

Created with: pug, CSS3, JS(ES6)  
[Repo](https://gitlab.com/syr1ken/js-syr1ken-projects/-/tree/main/domArrayMethods)  
[Demo](https://syr1ken.gitlab.io/js-syr1ken-projects/domArrayMethods/dist/index.html)

#### Exchange Rate Calculator

Created with: pug, CSS3, JS(ES6)  
[Repo](https://gitlab.com/syr1ken/js-syr1ken-projects/-/tree/main/exchangeRateCalculator)  
[Demo](https://syr1ken.gitlab.io/js-syr1ken-projects/exchangeRateCalculator/dist/index.html)

#### Hang Man Game

Created with: pug, CSS3, JS(ES6)  
[Repo](https://gitlab.com/syr1ken/js-syr1ken-projects/-/tree/main/hangManGame)  
[Demo](https://syr1ken.gitlab.io/js-syr1ken-projects/hangManGame/dist/index.html)
