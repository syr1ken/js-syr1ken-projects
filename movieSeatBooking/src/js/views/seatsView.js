import { elements, elementStrings } from "./../views/base";
import * as dataController from "./../dataController";

export const populateSeats = () => {
  const selectedSeats = dataController.getSeatsIndex();

  if (selectedSeats === null) return;
  if (selectedSeats.length === 0) return;

  elements.seats.forEach((seat, index) => {
    if (selectedSeats.indexOf(index) > -1) {
      seat.classList.add(elementStrings.selected);
    }
  });
};

export const populateOccupied = (state) => {
  if (state.occupiedSeats === null && state.occupiedSeats.length === 0) return;

  elements.seats.forEach((seat, index) => {
    if (state.occupiedSeats.indexOf(index) > -1) {
      seat.classList.add(elementStrings.occupied);
    }
  });
};
