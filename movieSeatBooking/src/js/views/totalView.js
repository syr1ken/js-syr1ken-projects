import { elements, getSelectedSeats } from "./../views/base";
import * as dataController from "./../dataController";

const updateCount = (element, num) => {
  element.innerHTML = Number(num);
};

const updateMoney = (element, money, count) => {
  element.innerHTML = `$${count * money}`;
};

export const updateTotal = (state) => {
  const seats = getSelectedSeats();

  state.totalCount = Number(seats.length);
  state.totalMoney = Number(elements.select.value);
  state.seatsIndex = [...getSelectedSeats()].map((seat) =>
    [...elements.seats].indexOf(seat)
  );

  updateCount(elements.count, state.totalCount);
  updateMoney(elements.money, state.totalMoney, state.totalCount);

  // Save array of selected seats in local storage
  dataController.setSeatsIndex(state.seatsIndex);
};
