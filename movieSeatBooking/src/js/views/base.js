export const elements = {
  theater: document.querySelector(".theater"),
  seats: document.querySelectorAll(".theater .seat:not(.occupied)"),
  count: document.querySelector(".total__count"),
  money: document.querySelector(".total__money"),
  select: document.querySelector(".select-box__select")
};

export const elementStrings = {
  selected: "seat--selected",
  occupied: "seat--occupied",
  seat: "seat",
  theater: "theater"
};

export const getSelectedSeats = () => {
  return document.querySelectorAll(
    `.${elementStrings.theater} .${elementStrings.seat}.${elementStrings.selected}`
  );
};
