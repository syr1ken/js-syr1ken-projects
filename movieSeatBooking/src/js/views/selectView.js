import { elements } from "./../views/base";
import * as dataController from "./../dataController";

export const getSelectedOption = () => {
  const selectData = dataController.getMovieData();

  if (selectData === null) return;

  elements.select.selectedIndex = selectData.selectedMovieIndex;
  return elements.select;
};
