export const setMovieData = (index, price) => {
  localStorage.setItem("selectedMovieIndex", index);
  localStorage.setItem("selectedMoviePrice", price);
};

export const setSeatsIndex = (seats) => {
  localStorage.setItem("selectedSeats", JSON.stringify(seats));
};

export const getSeatsIndex = () => {
  return JSON.parse(localStorage.getItem("selectedSeats"));
};

export const getMovieData = () => {
  const data = {
    selectedMovieIndex: localStorage.getItem("selectedMovieIndex"),
    selectedMoviePrice: localStorage.getItem("selectedMoviePrice")
  };
  return data;
};
