// Import Custom Styles
import "./../css/styles.css";

import { elements, elementStrings, getSelectedSeats } from "./views/base";
import * as totalView from "./views/totalView";
import * as seatslView from "./views/seatsView";
import * as selectView from "./views/selectView";
import * as dataController from "./dataController";

// App state initialization
const state = {
  totalCount: 0,
  totalMoney: 0,
  selectedMovie: elements.select,
  seatsIndex: [...getSelectedSeats()].map((seat) =>
    [...elements.seats].indexOf(seat)
  ),
  occupiedSeats: [1, 2, 3, 22, 23]
};

// Get data from local storage
seatslView.populateOccupied(state);
seatslView.populateSeats();
state.selectedMovie = selectView.getSelectedOption();
totalView.updateTotal(state);

// Event Handlers
const book = (e) => {
  e.preventDefault();

  if (
    e.target.classList.contains(elementStrings.seat) &&
    !e.target.classList.contains(elementStrings.occupied)
  ) {
    e.target.classList.toggle(elementStrings.selected);
    totalView.updateTotal(state);
  }
};

const changeOption = (e) => {
  e.preventDefault();

  state.selectedMovie = e.target;

  totalView.updateTotal(state);

  // Save selected movie to local storage
  dataController.setMovieData(
    state.selectedMovie.selectedIndex,
    state.selectedMovie.value
  );
};

// Event Listeners
elements.theater.addEventListener("click", book);
elements.select.addEventListener("change", changeOption);
