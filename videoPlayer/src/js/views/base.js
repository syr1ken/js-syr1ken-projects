export const elements = {
  video: document.querySelector(".video"),
  playBtn: document.querySelector(".play-btn"),
  stopBtn: document.querySelector(".stop-btn"),
  play: document.querySelector(".play"),
  stop: document.querySelector(".stop"),
  progress: document.querySelector(".progress"),
  timestamp: document.querySelector(".timestamp")
};

const icons = {
  play: require("../../img/play.svg"),
  pause: require("../../img/pause.svg"),
  stop: require("../../img/stop.svg")
};

const videos = {
  gone: require("../../videos/gone.mp4")
};

const images = {
  poster: require("../../img/poster.png")
};

const updateIcon = (element, path) => {
  element.src = path;
};

const updateTimestamp = (min, sec) => {
  elements.timestamp.innerHTML = `${min}:${sec}`;
};

export const updateIcons = () => {
  if (elements.video.paused) {
    updateIcon(elements.play, icons.play.default);
  } else {
    updateIcon(elements.play, icons.pause.default);
  }
};

export const toggleVideo = () => {
  if (elements.video.paused) {
    elements.video.play();
  } else {
    elements.video.pause();
  }
};

export const stopVideo = () => {
  elements.video.currentTime = 0;
  elements.video.pause();
  updateIcon(elements.play, icons.play.default);
};

export const updateProgress = () => {
  elements.progress.value =
    (elements.video.currentTime / elements.video.duration) * 100;

  let min = Math.floor(elements.video.currentTime / 60);
  if (min < 10) min = "0" + String(min);

  let sec = Math.floor(elements.video.currentTime % 60);
  if (sec < 10) sec = "0" + String(sec);

  updateTimestamp(min, sec);
};

export const setProgress = () => {
  elements.video.currentTime =
    (Number(elements.progress.value) * elements.video.duration) / 100;
};
