// Import Custom Styles
import "../css/styles.css";

import {
  elements,
  updateIcons,
  toggleVideo,
  stopVideo,
  updateProgress,
  setProgress
} from "./views/base";

// Event Listeners
// VIDEO
elements.video.addEventListener("click", toggleVideo);
elements.video.addEventListener("timeupdate", updateProgress);
elements.video.addEventListener("pause", updateIcons);
elements.video.addEventListener("play", updateIcons);

// CONTROLS
elements.play.addEventListener("click", toggleVideo);
elements.stop.addEventListener("click", stopVideo);

// PROGRESS BAR
elements.progress.addEventListener("change", setProgress);
