// Calculator contorller
var calcController = (function() {

  var displayValue = '0';

  var memmoryValue = 0;

  var resetDisplay = false;

  var pressedFunc = '';

  var checkDisplayLength = function() {
    displayValue = displayValue.toString();
    if (displayValue.length < 19) return true;
    return false;
  };

  var isInfiniteOrNaN =  function () {
    // Check if display value != infinite and != NaN
    if(!isFinite(displayValue) || isNaN(displayValue)) return true;
    return false;
  };

  return {
    hasDot: function (string) {
      if (string.includes('.')) return true;
      return false;
    },

    getResetDisplay: function() {
      return resetDisplay;
    },

    setResetDisplay: function(bool) {
      resetDisplay = bool;
    },

    getDisplayValue: function() {
      return displayValue;
    },

    getMemmoryValue: function() {
      return memmoryValue;
    },

    clearDisplayValue: function(bool) {
      displayValue = '';

      if (bool) {
        displayValue = '0';
      }
    },

    addDisplayValue: function(val) {
      // Check dispay lenght cause of calc witdh on browser window
      if (checkDisplayLength()) displayValue += val.toString();
    },

    back: function() {
      if (isInfiniteOrNaN()) return;
      // Remove last letter of displayValue
      var newValue = displayValue.slice(0, -1);

      // If last displayValue had 1 letter replace empty displayValue with 0
      if (newValue === '') {
        this.clearDisplayValue(true);
      } else {
        displayValue = newValue;
      }
    },

    clear: function() {
      this.clearDisplayValue(true);
    },

    clearAll: function() {
      this.clearDisplayValue(true);
      memmoryValue = 0;
      pressedFunc = '';
      resetDisplay = false;
    },

    pow: function() {
      resetDisplay = true;

      if (isInfiniteOrNaN()) return;
      displayValue = displayValue * displayValue;
      displayValue = displayValue.toString();
    },

    add: function() {
      resetDisplay = true;

      if (isInfiniteOrNaN()) return;
      // Set pressedFunc for eql function
      if(!pressedFunc) {
        memmoryValue = parseFloat(displayValue);
        pressedFunc = 'add';
        return;
      }

      displayValue = memmoryValue + parseFloat(displayValue);
      memmoryValue = parseFloat(displayValue);

      displayValue = displayValue.toString();
    },

    min: function() {

      resetDisplay = true;

      if (isInfiniteOrNaN()) return;
      // Set pressedFunc for eql function
      if(!pressedFunc) {
        memmoryValue = parseFloat(displayValue);
        pressedFunc = 'min';
        return;
      }

      displayValue = memmoryValue - parseFloat(displayValue);
      memmoryValue = parseFloat(displayValue);

      displayValue = displayValue.toString();
    },

    multiply: function() {
      resetDisplay = true;

      if (isInfiniteOrNaN()) return;
      // Set pressedFunc for eql function
      if(!pressedFunc) {
        memmoryValue = parseFloat(displayValue);
        pressedFunc = 'multiply';
        return;
      }

      displayValue = memmoryValue * parseFloat(displayValue);
      memmoryValue = parseFloat(displayValue);

      displayValue = displayValue.toString();
    },

    divide: function() {
      resetDisplay = true;

      if (isInfiniteOrNaN()) return;
      // Set pressedFunc for eql function
      if(!pressedFunc) {
        memmoryValue = parseFloat(displayValue);
        pressedFunc = 'divide';
        return;
      }

      displayValue = memmoryValue / parseFloat(displayValue);
      memmoryValue = parseFloat(displayValue);

      displayValue = displayValue.toString();
    },

    sqrt: function() {
      resetDisplay = true;

      if (isInfiniteOrNaN()) return;
      displayValue = Math.sqrt(parseFloat(displayValue));
      displayValue = displayValue.toString();
    },

    percent: function() {
      resetDisplay = true;

      if (isInfiniteOrNaN()) return;
      displayValue = parseFloat(displayValue) / 100;
      displayValue = displayValue.toString();
    },

    changeSign: function() {
      if (isInfiniteOrNaN()) return;
      var sign = Math.sign(parseFloat(displayValue))

      switch (sign) {
        case 0:
          displayValue = 0;
          break;
        case 1:
          displayValue = '-' + displayValue;
          break;
        case -1:
          displayValue = Math.abs(displayValue);
          break;
      }

      displayValue = displayValue.toString();
    },

    dot: function () {
      displayValue = displayValue.toString();
      if (resetDisplay) return;
      if (!this.hasDot(displayValue)) {
        displayValue = displayValue + '.';
      }
    },

    eql: function() {
      resetDisplay = false;

      this[pressedFunc]();

      pressedFunc = '';
    }
  };
})();

// UI controller
var UIController = (function() {
  var DOMstrings = {
    calcBtn: '.calc__btn',
    dispaly: '.dispaly',
  };

  return {
    getNumDataAttributes: function(element) {
      return {
        type: element.dataset.type,
        value: element.dataset.value
      }
    },

    getFuncDataAttributes: function(element) {
      return {
        type: element.dataset.type,
        funcName: element.dataset.funcname
      }
    },

    updateDisplayValue: function(value) {
      document.querySelector(DOMstrings.dispaly).textContent = value;
    },

    getDOMstrings: function() {
      return DOMstrings;
    }
  };
})();

// controller
var controller = (function(calcCtrl, UICtrl) {
  var setUpEventListeners = function () {
    var DOM = UICtrl.getDOMstrings();

    var buttons = Array.prototype.slice.call(document.querySelectorAll(DOM.calcBtn));

    buttons.forEach(function(current) {
      current.addEventListener('click', calcBtnCtrl)
    });
  };

  var processNumBtn = function (btn) {
    var clearDisplay = function() {
      // Clear display data number
      calcController.clearDisplayValue(false);
      // Replace 0 with new value
      calcController.addDisplayValue(btn.value);
      // Uodate UI with new display value
      UICtrl.updateDisplayValue(calcController.getDisplayValue());
    };

    // Check if btn.value != NaN or undefiend
    if(isNaN(btn.value) || typeof btn.value === 'undefined') return;

    // Get Display value
    var displayVal = parseFloat(calcController.getDisplayValue());

    // Check if length of display === 0 && btn.value === 0 && dispaly value doesn't contain dot
    if(displayVal === 0 && parseInt(btn.value) === 0 && !calcController.hasDot(calcController.getDisplayValue())) return;

    // Check if length of display === 0 and dispaly value doesn't contain dot
    if(displayVal === 0 && !calcController.hasDot(calcController.getDisplayValue())) {
      clearDisplay();
      return;
    }

    if (calcController.getResetDisplay()) {
      clearDisplay();
      calcController.setResetDisplay(false);
      return;
    }

    // Add new value to display controller value
    calcController.addDisplayValue(btn.value);
    // Uodate UI with new display value
    UICtrl.updateDisplayValue(calcController.getDisplayValue());
  };

  var processFuncBtn = function (btn) {
    // Process calculator function on calculator controller
    calcController[btn.funcName]();
    // Update display
    UICtrl.updateDisplayValue(calcController.getDisplayValue());
  };

  var calcBtnCtrl = function (event) {
    event.preventDefault();

    // Get clicked button data attributes
    if(event.target.dataset.type === 'num') {
      var clickedBtn = UICtrl.getNumDataAttributes(event.target);
      processNumBtn(clickedBtn);
    } else {
      var clickedBtn = UICtrl.getFuncDataAttributes(event.target);
      processFuncBtn(clickedBtn);
    }
  }


  return {
    init: function() {
      setUpEventListeners();

      // Uodate UI with new display value
      UICtrl.updateDisplayValue(calcController.getDisplayValue());;
    }
  };
})(calcController, UIController);

controller.init();
