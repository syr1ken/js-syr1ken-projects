// Import Custom Styles
import "../css/styles.css";

import {
  elements,
  elementStrings,
  error,
  success,
  getFieldName,
  removeState
} from "./views/base";

import Validate from "./assets/validate";

// Event Handlers
const validate = (e) => {
  e.preventDefault();

  const inputs = elements.form.querySelectorAll(elementStrings.input);
  let min, max;

  Array.prototype.slice.call(inputs).forEach((input) => {
    const errorEl = input.parentNode.querySelector(
      `.${elementStrings.errorElement}`
    );

    removeState(input, elementStrings.inputError);
    removeState(input, elementStrings.inputSuccess);
    removeState(errorEl, elementStrings.errorState);

    Validate.checkRequired(
      input,
      success,
      error,
      `${getFieldName(input)} is required`
    );
  });

  min = 3;
  max = 15;

  Validate.checkLength(
    elements.username,
    min,
    max,
    success,
    error,
    `Must be at least ${min} characters`,
    `Must be less than ${max} characters`
  );

  min = 6;
  max = 25;

  Validate.checkLength(
    elements.password,
    min,
    max,
    success,
    error,
    `Must be at least ${min} characters`,
    `Must be less than ${max} characters`
  );

  Validate.checkEmail(elements.email, success, error, `Enter valid email`);

  Validate.checkPassword(elements.password, success, error, `Invalid password`);

  Validate.checkPasswordMatch(
    elements.password,
    elements.passwordConfirm,
    success,
    error,
    `Passwords don't match`
  );
};

// Event Listeners
elements.form.addEventListener("submit", validate);
