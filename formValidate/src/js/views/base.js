export const elements = {
  form: document.querySelector(".form"),
  username: document.querySelector(".form__username"),
  email: document.querySelector(".form__email"),
  password: document.querySelector(".form__password"),
  passwordConfirm: document.querySelector(".form__password-confirm")
};

export const elementStrings = {
  inputSuccess: "form__input--success",
  inputError: "form__input--error",
  errorState: "form__error--error",
  errorElement: "form__error",
  input: "input"
};

export const error = (input, errorMessage) => {
  const error = input.parentNode.querySelector(
    `.${elementStrings.errorElement}`
  );
  showError(error, elementStrings.errorState, errorMessage);
  input.classList.add(elementStrings.inputError);
};

export const success = (input) => {
  input.classList.add(elementStrings.inputSuccess);
};

const showError = (error, className, message) => {
  error.innerHTML = message;
  error.classList.add(className);
};

export const removeState = (element, className) => {
  element.classList.remove(className);
};

export const getFieldName = (element) => {
  return element.name.charAt(0).toUpperCase() + element.name.slice(1);
};
