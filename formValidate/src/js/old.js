// Import Custom Styles
import "../css/styles.css";

import { elements, elementStrings } from "./views/base";

// View Controllers
const showError = (element, message) => {
  let error = element.parentNode.querySelector(
    `.${elementStrings.errorElement}`
  );
  error.innerHTML = message;
  error.classList.add(elementStrings.errorError);
  element.classList.add(elementStrings.inputError);
};

const showSuccess = (element) => {
  element.classList.add(elementStrings.inputSuccess);
};

const removeInputState = (element, className) => {
  const inputs = element.querySelectorAll("input");
  inputs.forEach((input) => {
    input.classList.remove(className);
  });
};

const removeErrorState = (element, className) => {
  element.classList.remove(className);
};

const isEmailValid = (email) => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

const isEmpty = (element) => {
  return element.value.toString().trim() === "";
};

const isValidPassword = (password) => {
  const re = /^[A-Za-z0-9\!@#$%^&*()_+=-`~\\\]\[{}|';:/.,?><]*$/;
  return re.test(password);
};

const getFieldName = (element) => {
  return element.name.charAt(0).toUpperCase() + element.name.slice(1);
};

const checkRequired = (element) => {
  if (isEmpty(element)) {
    showError(element, `${getFieldName(element)} is required`);
  } else {
    showSuccess(element);
  }
};

const checkLength = (element, min, max) => {
  if (element.value.length < min) {
    showError(element, `Must be at least ${min} characters`);
  } else if (element.value.length > max) {
    showError(element, `Must be less than ${max}`);
  } else {
    showSuccess(element);
  }
};

const checkInputLength = (element) => {
  switch (element.name.toLowerCase().trim()) {
    case "username":
      checkLength(element, 3, 15);
      break;
    case "password":
      checkLength(element, 6, 25);
      break;
    default:
      return;
  }
};

const checkEmail = (element) => {
  switch (element.name.toLowerCase().trim()) {
    case "email":
      if (!isEmailValid(element.value)) {
        showError(element, `Enter valid email`);
      } else {
        showSuccess(element);
      }
      break;
    default:
      return;
  }
};

const checkPassword = (element) => {
  if (!isValidPassword(element.value)) {
    showError(element, `Invalid password`);
  } else {
    showSuccess(element);
  }
};

const checkPasswordMatch = (element, elementConfitm) => {
  if (element.value !== elementConfitm.value) {
    showError(elementConfitm, `${getFieldName(element)} Passwords don't match`);
  } else {
    showSuccess(elementConfitm);
  }
};

// Event Handlers
const validate = (e) => {
  e.preventDefault();

  const inputs = elements.form.querySelectorAll(elementStrings.input);
  const errors = elements.form.querySelectorAll(
    `.${elementStrings.errorElement}`
  );

  removeInputState(elements.form, elementStrings.inputSuccess);
  removeInputState(elements.form, elementStrings.inputError);

  Array.prototype.slice.call(errors).forEach((element) => {
    removeErrorState(element, elementStrings.errorError);
  });

  Array.prototype.slice.call(inputs).forEach((element) => {
    checkRequired(element);
    checkInputLength(element);
    checkEmail(element);
  });

  checkPassword(elements.password);
  checkPasswordMatch(elements.password, elements.passwordConfirm);
};

// Event Listeners
elements.form.addEventListener("submit", validate);
