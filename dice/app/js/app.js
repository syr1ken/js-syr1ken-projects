// // Dice controller
var diceController = (function() {
  return {
    roll: function(max) {
      return Math.floor(Math.random() * max + 1);
    }
  }
})();

// UI Controller
var UIController = (function() {
  var DOMstrings = {
    meta: 'meta[data-colors]',
    logo: '.logo',
    linkTag: 'a',
    diceNumber: '.dice__number',
    rollBtn: '.btn-roll',
    select: '.select',
    selectOption: '.select__option'
  };

  var listForeach = function(list, callback) {
    for(var i = 0; i < list.length; i++) {
      callback(list[i], i);
    }
  };

  var colorsArr = [];

  var getAppsColors = function() {
    colors = document.querySelector(DOMstrings.meta).dataset.colors;

    colorsArr = colors.split(',');
  };

  var getRandomColor = function() {
    getAppsColors();

    var random = Math.floor(Math.random() * colorsArr.length);

    return colorsArr[random];
  };

  var removeAppsColors = function() {

    colorsArr.forEach(function(current) {
      current = current.replace(' ', '');

      document.querySelector(DOMstrings.logo).classList.remove(current);

      var elements = document.querySelectorAll(DOMstrings.linkTag);

      listForeach(elements, function(element) {
        element.classList.remove(current);
      });

      document.querySelector(DOMstrings.diceNumber).classList.remove(current);

      document.querySelector(DOMstrings.rollBtn).classList.remove(current + '-active');
    });
  };

  return {
    getSelectedOption: function() {
      var selected;

      selected = document.querySelector(DOMstrings.select).value;

      return selected;
    },

    displayNumber: function(num) {
      document.querySelector(DOMstrings.diceNumber).textContent = num;
    },

    changeAppsColors: function() {
      removeAppsColors();

      var color = getRandomColor();
      color = color.replace(' ', '');

      document.querySelector(DOMstrings.logo).classList.add(color);

      var elements = document.querySelectorAll(DOMstrings.linkTag);

      listForeach(elements, function(element) {
        element.classList.add(color);
      });

      document.querySelector(DOMstrings.diceNumber).classList.add(color);

      document.querySelector(DOMstrings.rollBtn).classList.add(color + '-active');
    },

    getDOMstrings: function() {
      return DOMstrings;
    }
  }
})();

// App controller
var controller = (function(diceCtrl, UICtrl) {
  var setUpEventListeners = function() {
    var DOM = UICtrl.getDOMstrings();

    document.querySelector(DOM.rollBtn).addEventListener('click', rollCtrl);

    document.querySelector(DOM.select).addEventListener('change', rollCtrl);
  };

  var rollCtrl = function(event) {
    event.preventDefault();

    // Find selected option
    var selected = UICtrl.getSelectedOption();
    // Roll
    var newNum = diceCtrl.roll(selected);
    // Display new number
    UICtrl.displayNumber(newNum);
    // Change apps color
    UICtrl.changeAppsColors();
  };

  return {
    init: function () {
      var diceFields;
      setUpEventListeners();
    }
  }
})(diceController, UIController);

controller.init();
