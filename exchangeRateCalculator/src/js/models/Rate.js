export default class Rate {
  constructor(currency) {
    this.currency = currency;
  }

  async getRate() {
    try {
      await fetch(`https://api.exchangerate-api.com/v4/latest/${this.currency}`)
        .then((res) => res.json())
        .then((data) => {
          this.data = data;
        });
    } catch (error) {
      console.log("Error", error.message);
    }
  }
}
