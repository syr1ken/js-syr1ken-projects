export const elements = {
  selectOne: document.querySelector(".select-one"),
  selectTwo: document.querySelector(".select-two"),
  inputOne: document.querySelector(".input-one"),
  inputTwo: document.querySelector(".input-two"),
  swap: document.querySelector(".swap"),
  rate: document.querySelector(".rate")
};

export const elementStrings = {};

export const images = {
  money: require("../../img/money.png")
};

export const updateRate = (cur, curTo, rate) => {
  elements.rate.innerHTML = `1 ${cur} = ${rate} ${curTo}`;
};

export const updateInput = (input, rate) => {
  input.value = (elements.inputOne.value * rate).toFixed(2);
};

export const getOption = (select) => {
  return select.value;
};
