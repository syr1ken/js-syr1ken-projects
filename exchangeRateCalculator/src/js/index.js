// Import Custom Styles
import "../css/styles.css";

// Views
import { elements, getOption, updateRate, updateInput } from "./views/base";
// Models
import Rate from "./models/Rate";

// Initialize App State
const state = {};

// Event Handlers
const controlRate = async () => {
  const currency = getOption(elements.selectOne);
  const currencyTo = getOption(elements.selectTwo);

  if (!currency) return;

  state.rate = new Rate(currency);

  try {
    await state.rate.getRate();
  } catch (error) {
    console.log("Error processing rate");
  }

  updateRate(currency, currencyTo, state.rate.data.rates[currencyTo]);
  updateInput(elements.inputTwo, state.rate.data.rates[currencyTo]);
};

const controlSwap = () => {
  const oldVal = getOption(elements.selectOne);
  elements.selectOne.value = elements.selectTwo.value;
  elements.selectTwo.value = oldVal;
  controlRate();
};

// Event Listeners
elements.selectOne.addEventListener("change", controlRate);
elements.selectTwo.addEventListener("change", controlRate);
elements.inputOne.addEventListener("input", controlRate);
elements.swap.addEventListener("click", controlSwap);
