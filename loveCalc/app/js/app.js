var inputController = (function() {

  var data = {
    placeholderNames: ['Anna', 'Chris', 'John', 'Li', 'Alice', 'Milena', 'Jenny', 'Rob', 'Pitter', 'Lois', 'Steve', 'Helen']
  };

  var isEmptyInput = false;

  return {
    getPlaceholderName: function() {
      return data.placeholderNames[Math.floor(Math.random()*data.placeholderNames.length)];
    },

    isEmpty: function(string) {
      return string === '';
    },

    setIsEmptyInput: function(bool) {
      if (typeof bool === "boolean") {
        isEmptyInput = bool;
      }
    },

    getIsEmptyInput: function() {
      return isEmptyInput;
    }
  };
})();

// UI controller
var UIController = (function() {
  var DOMstrings = {
    numberResult: '.heart__result',
    textResult: '.text__result',
    input: '.form-input',
    heart: '.heart',
    placeholder: 'placeholder'
  };

  return {
    fillPlaceholder: function(input, name) {
      input.setAttribute(DOMstrings.placeholder, name);
    },

    getInputsArr: function() {
      return Array.prototype.slice.call(document.querySelectorAll(DOMstrings.input));
    },

    removeStateClasses: function(inputs) {

      var textElem = document.querySelector(DOMstrings.textResult);

      textElem.textContent = '';
      textElem.classList.remove('red');
      textElem.classList.remove('green');

      inputs.forEach(function(current) {
        current.classList.remove('red-border');
      });
    },

    addEmptyError: function(input) {
      var textElem = document.querySelector(DOMstrings.textResult);

      textElem.textContent = 'Text inputs cannot be empty';
      textElem.classList.add('red');

      input.classList.add('red-border');
    },

    displayResultNumber: function(number) {
      document.querySelector(DOMstrings.numberResult).textContent = number;
    },

    displayResultText: function(number, names) {
      var textElem = document.querySelector(DOMstrings.textResult);

      textElem.classList.add('green');

      if (number >= 0 && number <= 25) {
        textElem.textContent = names[0].value + ' and ' + names[1].value + ' don\'t have anything in common.';
      } else if (number > 25 && number <= 50) {
        textElem.textContent = 'There is a chanse for ' + names[0].value + ' and ' + names[1].value + ' to be together but unlikely.';
      } else if (number > 50 && number <= 75) {
        textElem.textContent = names[0].value + ' and ' + names[1].value + ' more likely be together.';
      } else if (number > 75) {
        textElem.textContent = names[0].value + ' and ' + names[1].value + ' made for each other.';
      }
    },

    getDOMstrings: function() {
      return DOMstrings;
    }
  };
})();

// Controller
var controller = (function(inputCtrl, UICtrl) {

  // Find inputs fields
  var inputs = UICtrl.getInputsArr();

  var setUpEventListeners = function () {
    var DOM = UICtrl.getDOMstrings();

    document.querySelector(DOM.heart).addEventListener('click', clickCtrl);
  };

  var resetBools = function() {
    inputCtrl.setIsEmptyInput(false);
  };

  var clickCtrl = function(event) {
    event.preventDefault();

    // Reset bools
    resetBools();
    
    // Remove all state classes
    UICtrl.removeStateClasses(inputs);

    for(var i = 0; i < inputs.length; i++) {
      // Check if input is empty
      if (inputCtrl.isEmpty(inputs[i].value)) {
        UICtrl.addEmptyError(inputs[i]);
        inputCtrl.setIsEmptyInput(true);
      }
    }

    if(inputCtrl.getIsEmptyInput()) return;

    // Find new random number
    var random = Math.floor(Math.random() * 100 + 1);

    // Dispaly new number
    UICtrl.displayResultNumber(random);

    // Display text
    UICtrl.displayResultText(random, inputs);
  };

  return {
    init: function() {
      setUpEventListeners();

      // Fill every input with placeholder
      inputs.forEach(function(current) {
        UICtrl.fillPlaceholder(current, inputCtrl.getPlaceholderName());
      });
    }
  };

})(inputController, UIController);

controller.init();
