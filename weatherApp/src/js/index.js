// Import functions
function requireAll(r) { r.keys().forEach(r); }

// Require all images
requireAll(require.context('../img/', true, /\.(png|jpe?g|gif|svg|ico)$/i));

// Import bootstrap
import 'bootstrap/dist/js/bootstrap';

// Import custom styles
import '../sass/style.sass';

// Imports
import Search from './models/Search';
import * as searchView from './views/searchView'
import { elements, elementStrings, createLoader, removeLoader } from './views/base'

// Global var
const state = {};

// Search logic
const ctrlSearch = async () => {
  // Get input value
  const query = searchView.getInputValue();

  if (query) {
    state.search = new Search(query);

    // Prepare UI to res
    searchView.clearInput();
    searchView.clearDayForecast();
    searchView.clearWeekList();
    searchView.clearErrorText();
    elements.mainContent.classList.remove(elementStrings.active);

    // Render loader
    createLoader(elements.loaderContainer);

    try {
      // Get location data
      await state.search.getSearchData();

      if(state.search.result.length === 0 || state.search.result.length > 1) {
        const error = new Error('No results found');
        searchView.addFailText(error.message);
        throw error;
      }

      // Get city data
      await state.search.getLocationData(state.search.result[0].woeid);

      // Clear loader
      removeLoader();
      // Render results
      searchView.renderResults(state.search.title, state.search.week);

    } catch (e) {
      console.log('Error', e.message);
      // Clear loader
      removeLoader();
    }
  }
};

const ctrlClick = (event) => {
  const link = event.target.closest(`.${elementStrings.weekCard}`);

  if (link) {
    // Update week list UI
    const activeday = document.querySelector(`.${elementStrings.weekCardActive}`);
    if (activeday) activeday.classList.remove(elementStrings.weekCardActive);

    link.classList.add(elementStrings.weekCardActive);

    // Clear overview
    searchView.clearDayForecast();
    // Add loader
    createLoader(elements.loaderContainer);

    // Update day UI
    try {
      searchView.renderDay(state.search.title, state.search.week[link.dataset.id]);

      // Clear loader
      removeLoader();
    } catch (error) {
      console.log('Error', error.message);
      // Clear loader
      removeLoader();
    }
  }
};

elements.searchForm.addEventListener('submit', e => {
  e.preventDefault();
  ctrlSearch();
});

elements.weekList.addEventListener('click', ctrlClick)
