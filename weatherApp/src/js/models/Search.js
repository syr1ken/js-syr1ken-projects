import axios from 'axios';

export default class Search {
  constructor(query) {
    this.query = query;
  }

  async getSearchData() {
    try {
      const res = await axios(`https://cors-anywhere.herokuapp.com/https://www.metaweather.com/api/location/search/?query=${this.query}`);
      this.result = res.data;
    } catch (e) {
      console.log('Error', e.message);
    }
  }

  async getLocationData(woeid) {
    try {
      const res = await axios(`https://cors-anywhere.herokuapp.com/https://www.metaweather.com/api/location/${woeid}`);
      this.title = res.data.title;
      this.week = res.data.consolidated_weather;
    } catch (e) {
      console.log('Error', e.message);
    }
  }
}
