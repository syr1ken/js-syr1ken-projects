import { elements, elementStrings, consts } from './base'
import axios from 'axios'

export const getInputValue = () => elements.searchInput.value;

export const clearInput = () => {
  elements.searchInput.value = '';
};

export const clearWeekList = () => {
  elements.weekList.innerHTML = '';
};

export const clearDayForecast = () => {
  elements.forecastEl.innerHTML = '';
};

export const addFailText = (message) => {
  elements.error.textContent = message;
};

export const clearErrorText = () => {
  elements.error.textContent = '';
};

const createWeekCard = (day, index) => {
  let dateDay, dateMonth, dateYear, fullDate;

  const date = new Date(day.applicable_date);
  (date.getMonth() + 1).toString(10).length === 1 ? dateMonth = '0' + (date.getMonth() + 1).toString(10) : dateMonth = (date.getMonth() + 1).toString(10);
  (date.getDate() + 1).toString(10).length === 1 ? dateDay = '0' + (date.getDate() + 1).toString(10) : dateDay = (date.getDate() + 1).toString(10);
  dateYear = date.getFullYear();

  fullDate = `${dateDay}.${dateMonth}.${dateYear}`
  const weatherPic = `https://www.metaweather.com/static/img/weather/${day.weather_state_abbr}.svg`;

  const markup = `
  <div class="weekcard" data-id="${index}">
    <h2 class="weekcard__weekday">${consts.weekDays[date.getDay()]}</h2>
    <p class="weekcard__date">${fullDate}</p>
    <div class="weekcard__content" >
      <div class="weekcard__container">
        <img class="weekcard__icon" src="${weatherPic}" />
        <h4 class="weekcard__title">${day.weather_state_name}</h4>
      </div>
      <div class="weekcard__container">
        <div class="weekcard__avgtemp"><span>${Math.round(day.the_temp)}</span>&#8451;</div>
        <div class="weekcard__group">
          <div class="weekcard__temp__group">
            <div class="weekcard__temp__name">min</div>
            <div class="weekcard__temp">${Math.round(day.min_temp)}&#8451;</div>
          </div>
          <div class="weekcard__temp__group">
            <div class="weekcard__temp__name">max</div>
            <div class="weekcard__temp">${Math.round(day.max_temp)}&#8451;</div>
          </div>
        </div>
      </div>
      <div class="weekcard__container">
        <div class="weekcard__datagroup">
          <div class="weekcard__data">
            <div class="weekcard__data-name">Air speed</div>
            <div class="weekcard__data-num">${Math.round(day.wind_speed * 10) / 10} mph</div>
          </div>
          <div class="weekcard__data">
            <div class="weekcard__data-name">Pressure</div>
            <div class="weekcard__data-num">${Math.round(day.air_pressure)} mbar</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  `;

  return markup;
};

const createDayCard = (title, day) => {
  let dateDay, dateMonth, dateYear, fullDate;

  const date = new Date(day.applicable_date);
  const weatherPic = `https://www.metaweather.com/static/img/weather/${day.weather_state_abbr}.svg`;

  (date.getMonth() + 1).toString(10).length === 1 ? dateMonth = '0' + (date.getMonth() + 1).toString(10) : dateMonth = (date.getMonth() + 1).toString(10);
  (date.getDate() + 1).toString(10).length === 1 ? dateDay = '0' + (date.getDate() + 1).toString(10) : dateDay = (date.getDate() + 1).toString(10);
  dateYear = date.getFullYear();

  fullDate = `${dateDay}.${dateMonth}.${dateYear}`

  const markup = `
  <h2 class="forecast__title">${consts.weekDays[date.getDay()]}</h2>
  <hr class="line" />
  <div class="forecast__container">
    <p class="forecast__container-city">${title}</p>
    <p class="forecast__container-date">${fullDate}</p>
  </div>
  <div class="forecast__datalist">
    <div class="forecast__datalist-group">
      <div class="dataitem">
        <div class="dataitem__container">
          <h4 class="dataitem__name">Air speed</h4>
        </div>
        <div class="dataitem__container">
          <div class="dataitem__num">${Math.round(day.wind_speed * 10) / 10}</div>
          <div class="dataitem__unit">&nbsp;mph</div>
        </div>
      </div>
      <div class="dataitem">
        <div class="dataitem__container">
          <h4 class="dataitem__name">Pressure</h4>
        </div>
        <div class="dataitem__container">
          <div class="dataitem__num">${Math.round(day.air_pressure)}</div>
          <div class="dataitem__unit">&nbsp;mbar</div>
        </div>
      </div>
    </div>
    <div class="forecast__datalist-group">
      <div class="dataitem">
        <div class="dataitem__container">
          <h4 class="dataitem__name">Humidity</h4>
        </div>
        <div class="dataitem__container">
          <div class="dataitem__num">${Math.round(day.humidity)}</div>
          <div class="dataitem__unit">&nbsp;%</div>
        </div>
      </div>
      <div class="dataitem">
        <div class="dataitem__container">
          <h4 class="dataitem__name">Visibility</h4>
        </div>
        <div class="dataitem__container">
          <div class="dataitem__num">${Math.round(day.visibility * 100) / 100}</div>
          <div class="dataitem__unit">&nbsp;miles</div>
        </div>
      </div>
    </div>
  </div>
  <div class="forecast__overview">
    <div class="overview__iconcon"><img class="overview__icon" src="${weatherPic}" />
      <h4 class="overview__text">${day.weather_state_name}</h4>
    </div>
    <div class="overview__container">
      <div class="temperature__avgtemp"><span>${Math.round(day.the_temp)}</span>&#8451;</div>
      <div class="overview__group">
        <div class="temperature">
          <div class="temperature__header">min</div>
          <div class="temperature__degree">${Math.round(day.min_temp)}&#8451;</div>
        </div>
        <div class="temperature">
          <div class="temperature__header">max</div>
          <div class="temperature__degree">${Math.round(day.max_temp)}&#8451;</div>
        </div>
      </div>
    </div>
  </div>
  `;
  return markup;
};

export const renderDay = (title, day) => {
  try {
    if (title) {
      // Create day card
      const dayInfo = createDayCard(title, day);
      // Append to DOM
      elements.forecastEl.insertAdjacentHTML('afterbegin', dayInfo);
    }
  }
  catch (e) {
    console.log('Error:', e.message);
  }
};

export const renderResults = (title, week) => {
  try {
    if(title) {
      // Create week cards and append to DOM
      week.forEach((cur, index) => {
        const weekDay = createWeekCard(cur, index);
        elements.weekList.insertAdjacentHTML('beforeend', weekDay)
      });

      // Render today info
      renderDay(title, week[0]);

      // Add active to main content
      elements.mainContent.classList.add(elementStrings.active);
      
      // Add active to week day
      document.querySelector(`[data-id="${0}"]`).classList.add(elementStrings.weekCardActive);
    }
  } catch (e) {
    console.log('Error:', e.message);
  }
};
