export const elements = {
  searchForm: document.querySelector('.search'),
  searchInput: document.querySelector('.search__input'),
  mainContent: document.querySelector('.main__content'),
  weekList: document.querySelector('.week__list'),
  forecastEl: document.querySelector('.forecast'),
  loaderContainer: document.querySelector('.loader__container'),
  error: document.querySelector('.error')
};

export const consts = {
  weekDays: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
};

export const elementStrings = {
  active: 'active',
  weekCardActive: 'weekcard__active',
  loader: 'loader',
  weekCard: 'weekcard'
};

export const createLoader = parent => {
  const loader = `
  <div class="${elementStrings.loader}">
      <svg class="loader__img">
        <use href="img/icons.svg#icon-cw"></use>
      </svg>
    </div>
  `;
  parent.insertAdjacentHTML('afterbegin', loader);
}

export const removeLoader = () => {
  const loader = document.querySelector(`.${elementStrings.loader}`)
  if (loader) loader.parentElement.removeChild(loader);
}
