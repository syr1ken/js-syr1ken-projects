var app = {
  DOMstrings: {
    textarea: '.piglatin__textarea',
    text: '.piglatin__text',
    btn: '.button'
  },

  getTextareaContent: function() {
    return $(app.DOMstrings.textarea).val();
  },

  updateTextElement: function(text) {
    $(app.DOMstrings.text).text(text);
  },

  btnCtrl: function(e) {
    e.preventDefault();

    // Get textarea text
    var textareaContent = app.getTextareaContent();

    // Piglify or Englify
    var text = app[e.target.dataset.type](textareaContent);

    // Update text element
    app.updateTextElement(text);
  },

  piglify: function(text) {
    var regEx, words, resArr;
    resArr = [];

    // RegEx for vowels
    regEx = /^[aeiou]/i;
    // Get words array
    words = text.split(' ');

    words.forEach(function(word) {
      for(var i = 0; i < word.length; i++) {
        if (regEx.test(word[i]) && word.indexOf(word[i]) == 0) {
          resArr.push(word + 'way');
          break;
        }
        if (regEx.test(word[i])) {
          resArr.push(word.slice(word.indexOf(word[i]), word.legnth) + word.slice(0, word.indexOf(word[i])) + 'ay');
          break;
        }
        if (i === word.length - 1) {
          resArr.push(word);
        }
      }
    });

    return resArr.join(' ');
  },

  setUpEventListeners: function() {
    $(app.DOMstrings.btn).on('click', app.btnCtrl);
  },

  onReady: function() {
    app.setUpEventListeners();
  }
}

$(document).ready(app.onReady());
