export default class Person {
  constructor(firstName = "", lastName = "", wealth = 0) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.wealth = wealth;
  }
  async getPerson() {
    try {
      const res = await fetch("https://randomuser.me/api");
      const data = await res.json();
      this.firstName = data.results[0].name.first;
      this.lastName = data.results[0].name.last;
      this.wealth = Math.floor(Math.random() * 1000000);
    } catch (error) {
      console.log("Error", error.message);
    }
  }
}
