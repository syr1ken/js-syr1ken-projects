export const elements = {
  addPerson: document.querySelector(".add-person"),
  double: document.querySelector(".double"),
  showMillionaries: document.querySelector(".show_millionaries"),
  sort: document.querySelector(".sort"),
  calcWealth: document.querySelector(".calc_wealth"),
  main: document.querySelector(".main"),
  persons: document.querySelector(".persons"),
  total: document.querySelector(".total")
};

export const clearPersons = () => {
  elements.persons.innerHTML = "";
};

export const clearTotalWealth = () => {
  elements.total.innerHTML = "";
};

export const formatWealth = (wealth) => {
  return wealth.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,");
};
