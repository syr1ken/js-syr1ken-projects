import { elements, formatWealth } from "./base";

const createWealthEl = (wealth) => {
  const html = `
    <h4>Total Wealth</h4>
    <strong>$${formatWealth(wealth)}</strong>
  `;

  return html;
};

export const renderWealth = (wealth) => {
  if (!wealth) return;
  elements.total.insertAdjacentHTML("beforeend", createWealthEl(wealth));
};
