import { elements, formatWealth } from "./base";

const personStrings = {
  person: "person",
  personName: "person__name",
  personWealth: "person__wealth"
};

const createPerson = (person) => {
  const html = `
  <div class="${personStrings.person}">
    <p class="${personStrings.personName}">${person.firstName} ${
    person.lastName
  }</p>
    <span class="${personStrings.personWealth}">$${formatWealth(
    person.wealth
  )}</span>
  </div>`;

  return html;
};

export const renderPerson = (person) => {
  if (!person) return;
  elements.persons.insertAdjacentHTML("beforeend", createPerson(person));
};
