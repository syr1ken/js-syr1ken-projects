// Import Custom Styles
import "../css/styles.css";

import { elements, clearPersons, clearTotalWealth } from "./views/base";
import Person from "./models/Person";
import { renderPerson } from "./views/personView";
import { renderWealth } from "./views/totalWealth";

// Initialize state
const state = {
  persons: []
};

// Event Handlers
const controlAddPerson = async () => {
  try {
    const person = new Person();

    await person.getPerson();

    state.persons.push(person);

    renderPerson(person);
  } catch (error) {
    console.log(error);
  }
};

const controlDoubleWealth = () => {
  state.persons = state.persons.map((person) => {
    return new Person(person.firstName, person.lastName, person.wealth * 2);
  });

  // Clear UI
  clearPersons();

  //Render new UI
  state.persons.forEach((person) => renderPerson(person));
};

const controlSortByRichest = () => {
  state.persons = state.persons.sort((person, nextPerson) => {
    return nextPerson.wealth - person.wealth;
  });

  // Clear UI
  clearPersons();

  //Render new UI
  state.persons.forEach((person) => renderPerson(person));
};

const controlshowMillionaries = () => {
  state.persons = state.persons.filter((person) => {
    return person.wealth >= 1000000;
  });

  // Clear UI
  clearPersons();

  //Render new UI
  state.persons.forEach((person) => renderPerson(person));
};

const controlCalculateWealth = () => {
  const wealth = state.persons.reduce((total, person) => {
    return (total += person.wealth);
  }, 0);

  // Clear UI
  clearTotalWealth();

  //Render new UI
  renderWealth(wealth);
};

// Event Listeners
elements.addPerson.addEventListener("click", controlAddPerson);
elements.double.addEventListener("click", controlDoubleWealth);
elements.sort.addEventListener("click", controlSortByRichest);
elements.showMillionaries.addEventListener("click", controlshowMillionaries);
elements.calcWealth.addEventListener("click", controlCalculateWealth);
