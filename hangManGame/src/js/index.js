// Import Custom Styles
import "../css/styles.css";

// Views
import {
  elements,
  elementStrings,
  clearFigureParts,
  clearPopup,
  checkIfWin,
  checkIfLost,
  renderFigurePart,
  showNotification
} from "./views/base";
import * as wordView from "./views/wordView";
import * as wrongView from "./views/wrongView";

// Initialization
const state = {};

const initialize = () => {
  // Clear state
  state.words = ["application", "programming", "interface", "wizzard"];
  state.correctLetters = [];
  state.wrongLetters = [];

  // Clear popup
  clearPopup();

  // Clear Wrorng Letters Element
  wrongView.clearWrongLettersElement();
  // Clear all figures
  clearFigureParts();

  wordView.selectWord(state);
  wordView.renderWordElement(state);
};

initialize();

// EventHandlers
const pressKey = (e) => {
  if (
    elements.popupContainerElement.classList.contains(
      elementStrings.popupActive
    )
  )
    return;
  if (e.keyCode >= 65 && e.keyCode <= 90) {
    const key = e.key.toLowerCase();
    if (state.selectedWord.includes(key)) {
      if (!state.correctLetters.includes(key)) {
        state.correctLetters.push(key);
        console.log;
        wordView.renderWordElement(state);
        wordView.getGuessWord(state);
        checkIfWin(state);
      } else {
        showNotification(state);
      }
    } else {
      if (!state.wrongLetters.includes(key)) {
        state.wrongLetters.push(key);
        wrongView.renderWrongLettersElement(state);
        renderFigurePart(state);
        checkIfLost(state);
      } else {
        showNotification(state);
      }
    }
  }
};

// Event Listeners
window.addEventListener("keydown", pressKey);
elements.playElement.addEventListener("click", initialize);
