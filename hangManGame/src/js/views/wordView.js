import { elements } from "./base";

const createWordElement = (state) => {
  const html = `
    ${state.selectedWord
      .split("")
      .map(
        (letter) => `
    <span class="letter"> ${state.correctLetters.includes(letter) ? letter : ""}
    </span>
    `
      )
      .join("")}
  `;

  return html;
};

export const renderWordElement = (state) => {
  const word = createWordElement(state);
  elements.wordElement.innerHTML = word;
};

export const selectWord = (state) => {
  state.selectedWord =
    state.words[Math.floor(Math.random() * state.words.length)];
};

export const getGuessWord = (state) => {
  state.guessWord = elements.wordElement.innerText.replace(/\n/g, "");
};
