export const elementStrings = {
  word: "word",
  wrongLetters: "wrong-letters",
  play: "play",
  popupContainer: "popup__container",
  notificationContainer: "notification__container",
  message: "message",
  figurePart: "figure__part",
  notificationShow: "notification--show",
  popupActive: "popup--active",
  figureActive: "figure--active"
};

export const elements = {
  wordElement: document.querySelector(`.${elementStrings.word}`),
  wrongLettersElement: document.querySelector(
    `.${elementStrings.wrongLetters}`
  ),
  playElement: document.querySelector(`.${elementStrings.play}`),
  popupContainerElement: document.querySelector(
    `.${elementStrings.popupContainer}`
  ),
  notificationContainerElement: document.querySelector(
    `.${elementStrings.notificationContainer}`
  ),
  message: document.querySelector(`.${elementStrings.message}`),
  figureParts: document.querySelectorAll(`.${elementStrings.figurePart}`)
};

export const showNotification = (state) => {
  clearTimeout(state.notificationTimeout);
  elements.notificationContainerElement.classList.add(
    elementStrings.notificationShow
  );

  state.notificationTimeout = setTimeout(
    () =>
      elements.notificationContainerElement.classList.remove(
        elementStrings.notificationShow
      ),
    2000
  );
};

export const renderFigurePart = (state) => {
  elements.figureParts.forEach((part, index) => {
    if (index < state.wrongLetters.length) {
      part.classList.add(elementStrings.figureActive);
    } else {
      part.classList.remove(elementStrings.figureActive);
    }
  });
};

export const checkIfLost = (state) => {
  if (elements.figureParts.length === state.wrongLetters.length) {
    elements.popupContainerElement.classList.add(elementStrings.popupActive);
    elements.message.innerHTML = "You Have Lost";
  }
};

export const checkIfWin = (state) => {
  if (
    state.selectedWord.toString().toLowerCase() ===
    state.guessWord.toString().toLowerCase()
  ) {
    elements.popupContainerElement.classList.add(elementStrings.popupActive);
    elements.message.innerHTML = "You Have Won";
  }
};

export const clearPopup = () => {
  elements.message.innerHTML = "";
  elements.popupContainerElement.classList.remove(elementStrings.popupActive);
};

export const clearFigureParts = () => {
  elements.figureParts.forEach((part) => {
    part.classList.remove(elementStrings.figureActive);
  });
};
