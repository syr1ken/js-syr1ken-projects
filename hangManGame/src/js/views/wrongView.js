import { elements } from "./base";

const createWrongLettersElement = (state) => {
  const html = `
    ${state.wrongLetters.length > 0 ? `<p> Wrong </p>` : ""}
    ${state.wrongLetters.map((letter) => `<span> ${letter} </span>`)}
  `;

  return html;
};

export const renderWrongLettersElement = (state) => {
  const html = createWrongLettersElement(state);
  elements.wrongLettersElement.innerHTML = html;
};

export const clearWrongLettersElement = () => {
  elements.wrongLettersElement.innerHTML = "";
};
