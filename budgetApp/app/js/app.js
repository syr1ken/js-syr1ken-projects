// Budget controller
var budgetController = (function() {
  var Expense = function(id, description, value) {
    this.id = id;
    this.description = description;
    this.value = value;
  };

  var Income = function(id, description, value) {
    this.id = id;
    this.description = description;
    this.value = value;
  };

  var calculateTotal = function(type) {
    var sum;

    sum = 0;

    data.allItems[type].forEach(function(current, index, arr) {
      sum += current.value;
    });
    data.totals[type] = sum;
  };

  var data = {
    allItems: {
      exp: [],
      inc: []
    },
    totals: {
      exp: 0,
      inc: 0
    },
    budget: 0
  };

  return {
    addItem: function(type, des, val) {
      var newItem, ID;

      // Create new ID
      if (data.allItems[type].length > 0) {
        ID = data.allItems[type][data.allItems[type].length - 1].id + 1;
      } else {
        ID = 0;
      }
      // Create new item
      if (type === 'exp') {
        newItem = new Expense(ID, des, val);
      } else if (type === 'inc') {
        newItem = new Income(ID, des, val);
      }
      // Push item to data structure
      data.allItems[type].push(newItem);
      // Return new element
      return newItem;
    },

    deleteItem: function(type, id) {
      var ids, index;

      ids = data.allItems[type].map(function(current) {
        return current.id;
      });

      index = ids.indexOf(id);

      if (index !== -1) {
        data.allItems[type].splice(index, 1);
      }
    },

    calculateBudget: function() {
      // Calculate total income and expenses
      calculateTotal('exp');
      calculateTotal('inc');
      // Calculate the budget: income - expenses
      data.budget = data.totals.inc - data.totals.exp;
    },

    getBudget: function() {
      return {
        budget: data.budget,
        income: data.totals.inc,
        expense: data.totals.exp
      }
    }
  }
})();

// UI controller
var UIController = (function() {

  var DOMstrings = {
    inputType: '.add__type',
    inputDescription: '.add__description',
    inputValue: '.add__value',
    inputBtn: '.add__btn',
    incomeContainer: '.income__list',
    expenseContainer: '.expenses__list',
    budgetLabel: '.budget-top__value',
    incomeLabel: '.budget-top-income__value',
    expenseLabel: '.budget-top-expenses__value',
    container: '.budget-bottom',
    dateLabel: '.budget-top-tittle__month',
    logo: '.logo',
    linkTag: 'a'
  }

  var formatNumbers =  function(num, type) {
    num = Math.abs(num);
    num = num.toFixed(2);

    numSplit = num.split('.');

    int = numSplit[0];

    if (int.length > 3) {
      int = int.substr(0, int.length - 3) + ',' + int.substr(int.length - 3, 3);
    }

    dec = numSplit[1];

    return (type === 'exp' ? '-' : '+') + ' ' + int + '.' + dec
  };

  var nodeListForEach = function(list, callback) {
    for(var i = 0; i < list.length; i++) {
      callback(list[i], i);
    }
  };

  return {
    getInput: function() {
      return {
        type: document.querySelector(DOMstrings.inputType).value,
        description: document.querySelector(DOMstrings.inputDescription).value,
        value: parseFloat(document.querySelector(DOMstrings.inputValue).value)
      };
    },

    addListItem: function(obj, type) {
      var html, newHtml, element;
      // Create HTML string with placeholder text
      if(type === 'inc') {
        element = DOMstrings.incomeContainer;

        html = '<div class="item clearfix" id="inc-%id%"><div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div><div class="item__delete"><button class="item__delete--btn"><i class="far fa-times-circle"></i></button></div></div></div>';
      } else if (type === 'exp') {
        element = DOMstrings.expenseContainer;

        html = '<div class="item clearfix" id="exp-%id%"><div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div><div class="item__delete"><button class="item__delete--btn"><i class="far fa-times-circle"></i></button></div></div></div>';
      }
      // Replace placeholder text with data
      newHtml = html.replace('%id%', obj.id);
      newHtml = newHtml.replace('%description%', obj.description);
      newHtml = newHtml.replace('%value%', formatNumbers(obj.value, type));
      // Insert HTML into the DOM
      document.querySelector(element).insertAdjacentHTML('beforeend', newHtml);
    },

    deleteListItem: function(selectorID) {
      var element = document.getElementById(selectorID);

      element.parentNode.removeChild(element);
    },

    clearFields: function() {
      var fields, fieldsArr;

      fields = document.querySelectorAll(DOMstrings.inputDescription + ', ' + DOMstrings.inputValue);

      fieldsArr = Array.prototype.slice.call(fields);

      fieldsArr.forEach(function (current, index, arr) {
        current.value = '';
      });

      fieldsArr[0].focus();
    },

    displayBudget: function(obj) {

      var type;

      obj.budget > 0 ? type = 'inc' : type = 'exp';

      document.querySelector(DOMstrings.budgetLabel).textContent = formatNumbers(obj.budget, type);
      document.querySelector(DOMstrings.incomeLabel).textContent = formatNumbers(obj.income, 'inc');
      document.querySelector(DOMstrings.expenseLabel).textContent = formatNumbers(obj.expense, 'exp');
    },

    displayDate: function() {
      var now = new Date();

      month = now.getMonth();

      months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

      document.querySelector(DOMstrings.dateLabel).textContent = months[month] + ' ' + now.getFullYear();
    },

    changeType: function() {
      var fields = document.querySelectorAll(DOMstrings.inputType + ',' + DOMstrings.inputDescription + ',' + DOMstrings.inputValue);

      nodeListForEach(fields, function(current) {
        current.classList.toggle('red-focus');
      });

      document.querySelector(DOMstrings.inputBtn).classList.toggle('red');
    },

    changeAppColor: function(obj) {
      var logo, anchorElements;

      logo = document.querySelector(DOMstrings.logo);
      anchorElements = document.querySelectorAll(DOMstrings.linkTag);

      if (obj.budget < 0) {
        logo.classList.add('red');

        nodeListForEach(anchorElements, function(current) {
          current.classList.add('red-active');
        });
      } else {
        logo.classList.remove('red');

        nodeListForEach(anchorElements, function(current) {
          current.classList.remove('red-active');
        });
      }
    },

    trimInputValue: function() {
      var maxlength;

      maxlength = "6";
      console.log(this.value.length);

      if (this.value.length > maxlength) {
        this.value = this.value.slice(0, this.maxLength)
      }
    },

    getDOMstrings: function() {
      return DOMstrings;
    }
  };
})();

// Controller
var controller = (function(budgetCtrl, UICtrl) {
  var setupEventListeners = function() {
    var DOM = UIController.getDOMstrings();

    document.querySelector(DOM.inputBtn).addEventListener('click', ctrlAddItem);

    document.querySelector(DOM.container).addEventListener('click', ctrlDeleteItem);

    document.querySelector(DOM.inputType).addEventListener('change', UICtrl.changeType);

    document.querySelector(DOM.inputValue).addEventListener('input', UICtrl.trimInputValue);
  };

  var updateBudget = function() {
    // Calculate budget
    budgetCtrl.calculateBudget();
    // Return budget
    var budget = budgetCtrl.getBudget();
    // Display the budget on the UI
    UICtrl.displayBudget(budget);
    // Change app color to red or cyan
    UICtrl.changeAppColor(budget);
  };

  var ctrlAddItem = function() {
    // Get input data
    var input = UICtrl.getInput();

    if  (input.description !== '' && !isNaN(input.value) && input.value > 0) {
      // Add values to budget controller data structure
      var newItem = budgetCtrl.addItem(input.type, input.description, input.value);
      // Add item to the UI
      UICtrl.addListItem(newItem, input.type);
      // Clear the fields
      UICtrl.clearFields();
      // Calculate and update budget
      updateBudget();
    }
  };

  var ctrlDeleteItem  = function(event) {
    var itemID, splitID, type, id;

    itemID = event.target.parentNode.parentNode.parentNode.parentNode.id;

    if (itemID) {
      splitID = itemID.split('-');
      type = splitID[0];
      id = parseInt(splitID[1]);

      // Delete item from data structure
      budgetCtrl.deleteItem(type, id);
      // Delete the item from the UI
      UICtrl.deleteListItem(itemID);
      // Update budget
      updateBudget();
    }
  }

  return {
    init: function() {
      setupEventListeners();
      UICtrl.displayDate();
      UICtrl.displayBudget({
        budget: 0,
        income: 0,
        expense: 0
      });
    }
  }

})(budgetController, UIController);

controller.init();
