// Import Font Awesome Styles
import "@fortawesome/fontawesome-free/js/all";

// Import Custom Styles
import "../css/styles.css";

const elements = {
  toggle: document.querySelector(".toggle"),
  close: document.querySelector(".close"),
  cta: document.querySelector(".cta"),
  modalContainer: document.querySelector(".modal__container")
};

const elementStrings = {
  showNav: "show-nav",
  modalActive: "modal--active"
};

const controlToggle = () => {
  document.body.classList.toggle(elementStrings.showNav);
};

const controlShowModal = () => {
  elements.modalContainer.classList.add(elementStrings.modalActive);
};

const controlCloseModal = () => {
  elements.modalContainer.classList.remove(elementStrings.modalActive);
};

// Event Listeners
elements.toggle.addEventListener("click", controlToggle);
elements.cta.addEventListener("click", controlShowModal);
elements.close.addEventListener("click", controlCloseModal);
window.addEventListener("click", (e) =>
  e.target === elements.modalContainer
    ? elements.modalContainer.classList.remove(elementStrings.modalActive)
    : false
);
