// Import functions
function requireAll(r) { r.keys().forEach(r); }

// Require all images
requireAll(require.context('../img/', true, /\.(png|jpe?g|gif|svg|ico)$/i));

// Import bootstrap
import 'bootstrap/dist/js/bootstrap';
// import 'bootstrap/dist/css/bootstrap.css';

// Import custom styles
import '../sass/style.sass';

import Search from './models/Search';
import Recipe from './models/Recipe';
import * as searchView from './views/searchView';
import * as recipeView from './views/recipeView';
import { elements, renderLoader, clearLoader, resultLinksClearActive, elementsString } from './views/base';

// Global state of the app
const state = {};

const controlSearch = async () => {
  // Get query from view
  const query = searchView.getInput();

  // Create new Search obj
  if (query) {
    state.search = new Search(query);

    // Prepare UI for results
    searchView.clearInput();
    searchView.clearResults();

    // Render loader
    renderLoader(elements.searchResult);

    try {
      // Search for recipes
      await state.search.getResults();

      // Render results on UI
      clearLoader();
      resultLinksClearActive();
      searchView.renderResults(state.search.result);

    } catch (err) {
      console.log('Error processing recipe!');
      clearLoader();
    }
  }
};

elements.searchForm.addEventListener('submit', e => {
  e.preventDefault();
  controlSearch();
});

elements.resultPages.addEventListener('click', e => {
  const button = e.target.closest('.pages__btn');

  if (button) {
    const goToPage = parseInt(button.dataset.goto, 10);

    searchView.clearResults();
    searchView.renderResults(state.search.result, goToPage);
  }
});

const controlRecipe = async (e) => {
  const link = e.target.closest('.results__link');

  if (link) {
    // Prepare UI for changes
    renderLoader(elements.recipeContent);
    recipeView.clearRecipe();
    resultLinksClearActive();
    link.classList.add(elementsString.resultLinksClearActive);

    // Create new recipe objet
    state.recipe = new Recipe(link.dataset.resid);

    try {
      // Get recipe data
      await state.recipe.getRecipe();
      // Render recipe
      clearLoader();
      recipeView.renderRecipe(state.recipe);
    } catch (err) {
      console.log('Error processing recipe!', err);
      clearLoader();
    }
  }
};

elements.searchResult.addEventListener('click', controlRecipe);
