import { elements, elementsString } from './base';

export const getInput = () => elements.searchInput.value;

export const clearInput = () => {
  elements.searchInput.value = '';
};

export const clearResults = () => {
  elements.searchResultList.innerHTML = '';
  elements.resultPages.innerHTML = '';
};

const limitRecipeTitle = (title, limit = 35) => {
  const newTitle = [];

  if(title.length > limit) {
    title.split(' ').reduce((acc, cur) => {
      if(acc + cur.length <= limit) {
        newTitle.push(cur);
      }
      return acc + cur.length;
    }, 0);
    return `${newTitle.join(' ')} ...`
  }
  return title;
};

const renderRecipe = recipe => {
  const markup = `
  <li>
    <a href="#recipe" class="results__link" data-resid="${recipe.recipe_id}">
      <figure class="results__fig">
        <img src="${recipe.image_url}" alt="${recipe.title}" class="results__img">
      </figure>
      <div class="results__data">
        <h4 class="results__name">${limitRecipeTitle(recipe.title)}</h4>
        <p class="results__author">${recipe.publisher}</p>
      </div>
    </a>
  </li>
  `;

  elements.searchResultList.insertAdjacentHTML('beforeend', markup);
};

const createButton = (page, type) => `
  <a href="#results" class="pages__btn results__btn--${type}" data-goto=${type == 'prev' ? page - 1 : page + 1}>
    <svg class="results__icon">
      <use href="img/icons.svg#icon-triangle-${type == 'prev' ? 'left' : 'right'}"></use>
    </svg>
  </a>
`;

export const renderButtons = (page, pages) => {
  let buttons;
  if (pages > 1) {
    buttons = `
    ${createButton(page, 'prev')}
    <div class="results__num">${page}</div>
    ${createButton(page, 'next')}
    `;
  }
  elements.resultPages.insertAdjacentHTML('afterbegin', buttons);
};

export const renderResults = (recipes, page = 1, resPerPage = 5) => {
  if (!recipes) {
    //Remove active state
    elements.searchResult.classList.remove(elementsString.resultPagesActive);
    return;
  };

  const pages = Math.ceil(recipes.length / resPerPage);

  if(page === pages + 1) page = 1;
  if(page === 0) page = pages;

  // Render results of current page
  const start = (page - 1) * resPerPage;
  const end = page * resPerPage

  //Add active state
  elements.searchResult.classList.add(elementsString.resultPagesActive);

  recipes.slice(start, end).forEach(renderRecipe);
  // Render pagination
  renderButtons(page, pages);
};
