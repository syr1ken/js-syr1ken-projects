import { elements, elementsString } from './base';

const createRecipeHeader = (recipe) => {
  const header = `
    <figure class="recipe__fig">
      <img src="${recipe.img}" alt="${recipe.title}" class="recipe__img">
      <hr>
      <h1 class="recipe__title">${recipe.title}</h1>
      <hr>
    </figure>
  `
  return header;
};

const createIngredientsItem = (ingredient) => {
  const listItem = `
    <li class="recipe__item">
      <svg class="recipe__icon">
        <use href="img/icons.svg#icon-check"></use>
      </svg>
      <div class="recipe__text">${ingredient}</div>
    </li>
  `
  return listItem;
};

export const clearRecipe = () => {
  elements.recipeHeader.innerHTML = '';
  elements.ingredientsList.innerHTML = '';
};

export const renderRecipe = recipe => {
  if(!recipe) return;

  const header = `
  ${createRecipeHeader(recipe)}
  `;

  let ingredients = ``;

  recipe.ingredients.forEach(el => {
    ingredients += createIngredientsItem(el);
  });

  elements.recipeHeader.insertAdjacentHTML('afterbegin', header);
  elements.ingredientsList.insertAdjacentHTML('afterbegin', ingredients);
};
