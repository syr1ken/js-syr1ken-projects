export const elements = {
  searchForm: document.querySelector('.search'),
  searchInput: document.querySelector('.search__field'),
  searchResultList: document.querySelector('.results__list'),
  searchResult: document.querySelector('.results__content'),
  resultPages: document.querySelector('.results__pages'),
  recipeContent: document.querySelector('.recipe__content'),
  recipeHeader: document.querySelector('.recipe__header'),
  ingredientsList: document.querySelector('.recipe__ingredient-list'),
};

export const elementsString = {
  loader: 'loader',
  resultPagesActive: 'results__pages--active',
  resultLinksClearActive: 'results__link--active'
};

export const resultLinksClearActive = () => {
  Array.prototype.slice.call(document.querySelectorAll('.results__link')).forEach(el => {
    el.classList.remove(elementsString.resultLinksClearActive);
  });
};

export const renderLoader = parent => {
  const loader = `
    <div class="${elementsString.loader}">
      <svg class="loader__img">
        <use href="img/icons.svg#icon-cw"></use>
      </svg>
    </div>
  `;

  parent.insertAdjacentHTML('afterbegin', loader);
};

export const clearLoader = () => {
  const loader = document.querySelector(`.${elementsString.loader}`);
  if(loader) loader.parentElement.removeChild(loader);
};
